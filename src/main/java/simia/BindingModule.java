package simia;

import com.google.inject.AbstractModule;
import lombok.RequiredArgsConstructor;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;
import simia.api.GameService;
import simia.domain.Configuration;

@RequiredArgsConstructor
public class BindingModule extends AbstractModule {
    private final Configuration configuration;

    @Override
    protected void configure() {
        Retrofit retrofit = getRetrofitBuilder();
        bind(Configuration.class).toInstance(configuration);
        bind(GameService.class).toInstance(retrofit.create(GameService.class));
    }

    private Retrofit getRetrofitBuilder() {
        return new Retrofit.Builder()
                .addConverterFactory(JacksonConverterFactory.create())
                .baseUrl(configuration.getBaseUrl())
                .build();
    }
}
