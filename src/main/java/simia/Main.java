package simia;

import com.google.inject.Guice;
import com.google.inject.Injector;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.fxml.LoadException;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import simia.controllers.StartScreenController;
import simia.domain.Configuration;
import simia.service.FxmlLoaderService;
import simia.service.GameScreenService;
import simia.service.YamlConfigService;

import java.io.IOException;

public class Main extends Application {


    public static void main(String[] args) throws IOException {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Configuration config = YamlConfigService.loadConfig("config.yml");
        Injector injector = Guice.createInjector(new BindingModule(config));
        // Open FXML in new Window
        primaryStage.setTitle("Rating Guesser");
        StartScreenController controller = new StartScreenController(injector.getInstance(GameScreenService.class), config);
        Parent startParent = FxmlLoaderService.loadFXML("/simia/view/start-screen.fxml", controller, getClass());
        Scene scene = new Scene(startParent, 600, 400);
        // add CSS for styling
        scene.getStylesheets().add("/simia/css/style.css");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
