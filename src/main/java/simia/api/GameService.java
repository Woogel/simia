package simia.api;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import simia.domain.Game;

import java.util.List;

public interface GameService {
    @GET("games/{gameID}?fields=name, rating, summary")
    Call<List<Game>> getGame(@Path("gameID") int gameID, @Header("user-key") String authorization);
}
