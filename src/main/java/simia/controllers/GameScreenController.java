package simia.controllers;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;
import lombok.RequiredArgsConstructor;
import simia.api.GameService;
import simia.domain.Configuration;
import simia.domain.Game;
import simia.service.FinishScreenService;
import simia.service.FxmlLoaderService;
import simia.service.GameScreenService;

import java.io.IOException;
@RequiredArgsConstructor
public class GameScreenController {

    private static final int AMOUNT_OF_GAMES = 100;
    private final GameScreenService gameScreenService;
    private final Configuration configuration;

    private Game gameA;
    private Game gameB;
    private int points;
    private int timeLeftInSeconds;
    private Timeline timeline;

    @FXML
    private
    Label timer;

    @FXML
    private
    AnchorPane gameScreenAnchorPane;

    @FXML
    private Label pointsField;

    @FXML
    private Label titleA;

    @FXML
    private Label titleB;

    @FXML
    private Label summaryA;

    @FXML
    private Label summaryB;

    @FXML
    public void initialize() {
        points = 0;
        timeLeftInSeconds = configuration.getTimeInSeconds();
        updateUI();
        startTimer();
    }

    private void mapGameToUi(Game game, Label title, Label summary) {
        // displays given value on the UI
        title.setText(game.getName());
        summary.setText(game.getSummary());
    }

    @FXML
    private void guessA() {
        // User guesses game A
        if (gameA.getRating() >= gameB.getRating()) {
            points++;
        } else {
            points--;
        }
        updateUI();
    }

    @FXML
    private void guessB() {
        // User guesses game B
        if (gameB.getRating() >= gameA.getRating()) {
            points++;
        } else {
            points--;
        }
        updateUI();
    }

    private void updateUI() {
        pointsField.setText(FinishScreenService.formatPoints(points));
        try {
            // Fetches random game from the UI
            gameA = gameScreenService.getGame((int) (Math.random() * AMOUNT_OF_GAMES + 1));
            gameB = gameScreenService.getGame((int) (Math.random() * AMOUNT_OF_GAMES + 1));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        // Display two random games in the UI
        mapGameToUi(gameA, titleA, summaryA);
        mapGameToUi(gameB, titleB, summaryB);
    }

    private void finishGame() {
        // Stop the still running timeline job
        timeline.stop();
        // Load finish screen
        Parent gameScreen;
        FinishScreenController finishScreenController = new FinishScreenController(gameScreenService, configuration, points);
        try {
            gameScreen = FxmlLoaderService.loadFXML("/simia/view/finish-screen.fxml", finishScreenController, getClass());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        gameScreenAnchorPane.getChildren().setAll(gameScreen);
    }

    private void startTimer() {
        timer.setText(GameScreenService.formatTime(timeLeftInSeconds));
        // Create job to count down timer every second
        timeline = new Timeline(
                new KeyFrame(Duration.seconds(1), event -> {
                    timeLeftInSeconds--;
                    timer.setText(GameScreenService.formatTime(timeLeftInSeconds));
                    if (timeLeftInSeconds <= 0) {
                        finishGame();
                    }
                }));
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();
    }

}
