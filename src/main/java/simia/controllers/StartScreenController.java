package simia.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.AnchorPane;
import lombok.RequiredArgsConstructor;
import simia.domain.Configuration;
import simia.service.FxmlLoaderService;
import simia.service.GameScreenService;

import javax.inject.Inject;
import java.io.IOException;

@RequiredArgsConstructor(onConstructor=@__({@Inject}))
public class StartScreenController {

    @FXML
    private AnchorPane startScreenAnchorPane;

    private final GameScreenService gameScreenService;

    private final Configuration configuration;

    @FXML
    private void startGame() {
        Parent gameScreen;
        try {
            GameScreenController controller = new GameScreenController(gameScreenService, configuration);
            gameScreen = FxmlLoaderService.loadFXML("/simia/view/game-screen.fxml", controller, getClass());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        startScreenAnchorPane.getChildren().setAll(gameScreen);
    }

}
