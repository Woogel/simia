package simia.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import lombok.RequiredArgsConstructor;
import simia.domain.Configuration;
import simia.domain.Game;
import simia.service.FinishScreenService;
import simia.service.FxmlLoaderService;
import simia.service.GameScreenService;

import java.io.IOException;

@RequiredArgsConstructor
public class FinishScreenController {
    @FXML
    private Label endResult;

    @FXML
    AnchorPane finishScreenAnchorPane;

    final GameScreenService gameScreenService;
    final Configuration configuration;

    private final int points;

    @FXML
    public void initialize() {
        endResult.setText(FinishScreenService.formatPoints(points));
    }

    @FXML
    public void replay() {
        Parent gameScreen;
        try {
            GameScreenController controller = new GameScreenController(gameScreenService, configuration);
            gameScreen = FxmlLoaderService.loadFXML("/simia/view/game-screen.fxml", controller, getClass());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        finishScreenAnchorPane.getChildren().setAll(gameScreen);
    }
}
