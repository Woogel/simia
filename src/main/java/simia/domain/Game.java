package simia.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.io.Serializable;

@Data
public class Game implements Serializable {
    private int id;
    private String name;
    private String summary;
    private float rating;
}
