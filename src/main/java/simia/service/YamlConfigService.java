package simia.service;

import org.yaml.snakeyaml.Yaml;
import simia.domain.Configuration;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public class YamlConfigService {

    public static Configuration loadConfig(String path) throws IOException {
        // Load and parse YAML config file from desired location
        Yaml yaml = new Yaml();
        InputStream in = Files.newInputStream(Paths.get(path));
        return yaml.loadAs(in, Configuration.class);
    }
}
