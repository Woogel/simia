package simia.service;

public class FinishScreenService {

    public static String formatPoints(int points) {
        return String.format("Points: %d", points);
    }
}
