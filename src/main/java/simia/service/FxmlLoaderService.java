package simia.service;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

import java.io.IOException;

public class FxmlLoaderService {
    public static Parent loadFXML(String path, Object controller, Class callingClass) throws IOException {
        Parent parent;
        FXMLLoader loader = new FXMLLoader(callingClass.getResource(path));
        loader.setController(controller);
        parent = loader.load();
        return parent;
    }
}
