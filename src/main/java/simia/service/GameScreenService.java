package simia.service;

import lombok.RequiredArgsConstructor;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;
import simia.api.GameService;
import simia.domain.Configuration;
import simia.domain.Game;

import javax.inject.Inject;
import java.io.IOException;
import java.util.List;

@RequiredArgsConstructor(onConstructor=@__({@Inject}))
public class GameScreenService {

    private final Configuration configuration;
    private final GameService gameService;

    public Game getGame(int gameID) throws IOException {
        Response<List<Game>> response = gameService.getGame(gameID, configuration.getApiToken()).execute();
        return response.body().get(0);
    }

    public static String formatTime(int timeLeftInSeconds) {
        return String.format("Time: %d sec", timeLeftInSeconds);
    }
}
