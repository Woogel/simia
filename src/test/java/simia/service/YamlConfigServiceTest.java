package simia.service;

import org.junit.Test;
import simia.domain.Configuration;

import java.io.IOException;

import static org.junit.Assert.*;

public class YamlConfigServiceTest {

    @Test
    public void shouldLoadConfig() throws IOException {
        Configuration ownConfig = Configuration.builder()
                .apiToken("vhfsuipai3obhoifbdhosoa")
                .baseUrl("https://api-v3.igdb.com/")
                .timeInSeconds(30)
                .build();

        Configuration configuration = YamlConfigService.loadConfig("src/test/resources/simia/files/config.yml");
        assertEquals(ownConfig, configuration);
    }
}