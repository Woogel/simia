package simia.service;

import org.junit.Test;

import static org.junit.Assert.*;

public class GameScreenServiceTest {

    @Test
    public void shouldFormatTime() {
        int timeInSeconds = 30;
        String formattedTime = GameScreenService.formatTime(timeInSeconds);
        assertEquals("Time: 30 sec", formattedTime);
    }
}