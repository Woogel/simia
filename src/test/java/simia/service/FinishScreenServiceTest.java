package simia.service;

import static org.junit.Assert.*;

public class FinishScreenServiceTest {

    @org.junit.Test
    public void shouldFormatPoints() {
        int points = 5;
        String formattedPoints = FinishScreenService.formatPoints(5);
        assertEquals("Points: 5", formattedPoints);
    }
}