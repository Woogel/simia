# simia

A JavaFx Mini Game working with the [IGDB API](https://igdb.github.io/api/endpoints/game/)

## Game Rules

Once you start a game, you get to a screen with two video games. You have  to decide, which video game
has a higher rating. If you're right, you get a point. If your wrong, you get one point withdrawn. 
After every guess, two new games get loaded. The game stops after 20 seconds.

## Getting Started

1. Clone the Git-Repository to your desired location.
2. Open a command line and step into the project.
3. Run `./gradlew run` to start the application.

### Developping in IntelliJ

1. Install the IntelliJ Lombok Plugin
2. Enable Annotation Processors

### Prerequisites

- A Java JDK (worked on 1.8)

## Running the tests

1. Step into the project and run `./gradlew test` to run all JUnit tests.

## Built With

* [Gradle](https://gradle.org) - Dependency Management and Build Tool

## Authors

* **Ivo Cavelti** - TBZ - AP17a

